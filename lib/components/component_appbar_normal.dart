import 'package:flutter/material.dart';

class ComponentAppbarNormal extends StatefulWidget {
  const ComponentAppbarNormal({
    super.key,
    required this.title
  });

  final String title;

  @override
  State<ComponentAppbarNormal> createState() => _ComponentAppbarNormalState();
}

class _ComponentAppbarNormalState extends State<ComponentAppbarNormal> {
  @override
  Widget build(BuildContext context) {
    return AppBar(
      title: Text(widget.title) ,
    );
  }
}
