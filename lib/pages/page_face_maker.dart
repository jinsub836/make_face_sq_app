import 'package:flutter/material.dart';

class PageFaceMaker extends StatefulWidget {
  const PageFaceMaker({
    super.key,
    required this.friendName
  });

  final friendName;

  @override
  State<PageFaceMaker> createState() => _PageFaceMakerState();
}

class _PageFaceMakerState extends State<PageFaceMaker> {
  @override
  Widget build(BuildContext context) {
    return Container(
      child: Text(widget.friendName),
    );
  }
}
