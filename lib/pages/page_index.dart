import 'package:flutter/material.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:form_builder_validators/form_builder_validators.dart';
import 'package:make_face_sq_app/pages/page_face_maker.dart';


class PageIndex extends StatefulWidget {
  const PageIndex({super.key});

  @override
  State<PageIndex> createState() => _PageIndexState();
}

class _PageIndexState extends State<PageIndex> {
  final _formKey = GlobalKey<FormBuilderState>();
  final _emailFieldKey = GlobalKey<FormBuilderFieldState>();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('data'),
      ),
      body: _buildBody(context),
    );
  }

    Widget _buildBody(BuildContext context){
      return SingleChildScrollView(
        child: Container(  margin: EdgeInsets.only(top: 100),
          child:Center(
          child: FormBuilder(
            key: _formKey,
            child: Column(
              children: [
                Container( width: 350, margin: EdgeInsets.only(top: 100),
                  child:
                FormBuilderTextField(
                  name: 'friendName',
                  decoration: const InputDecoration(labelText: '이름을 입력하세요'),
                  validator: FormBuilderValidators.compose([
                    FormBuilderValidators.required(),
                  ]),
                ),),
                Container( margin: EdgeInsets.only(top: 60),
                  child:
                MaterialButton(
                  color: Theme.of(context).colorScheme.primary,
                  onPressed: () {
                    if (_formKey.currentState?.saveAndValidate() ?? false){
                      String friendName = _formKey.currentState?.fields['friendName']!.value;
                      Navigator.push(context,
                      MaterialPageRoute(builder:
                      (context)=> PageFaceMaker(friendName: friendName)));
                    };
                  },
                  child: const Text('시작'),
                )
              ,),
                Container( width: 350, height: 390,
                  child:Text(''),
                  decoration: BoxDecoration(
                    image: DecorationImage
                      (image: AssetImage('assets/startback.png'),
                        fit:BoxFit.cover
                    ),
                  ),)],
            ),
          ),
        ),),
      );}}
